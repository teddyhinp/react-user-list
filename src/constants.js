export const sortOptions = [
  { label: 'Name', value: 'name' },
  { label: 'Email', value: 'email' },
  { label: 'Phone Number', value: 'phone' },
];
export const orderOptions = [
  { label: 'Asc', value: 'asc' },
  { label: 'Desc', value: 'desc' },
];

export const pageSizeOptions = [
  { label: '1', value: 1 },
  { label: '2', value: 2 },
  { label: '5', value: 5 },
  { label: '10', value: 10 },
];
