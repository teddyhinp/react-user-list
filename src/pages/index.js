import Controller from '@/components/Controller';
import UserList from '@/components/UserList';
import styled from '@emotion/styled';
import { Box } from '@mui/material';

export default function Home() {
  return (
    <PageContainer>
      <UserList />
      <Controller />
    </PageContainer>
  );
}

const PageContainer = styled(Box)({
  maxWidth: '1200px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  margin: 'auto',
});
