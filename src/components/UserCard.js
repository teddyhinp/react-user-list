import styled from '@emotion/styled';
import { Box } from '@mui/material';
import React from 'react';

export default function UserCard({ detail, index }) {
  const { id, name, email, phone } = detail;
  return (
    <StyledCard index={index}>
      <Box className="userName">{name}</Box>
      <Row>
        <Box className="label">Email: </Box>
        <Box>{email}</Box>
      </Row>
      <Row>
        <Box className="label">Phone:</Box>
        <Box>{phone}</Box>
      </Row>
    </StyledCard>
  );
}

const StyledCard = styled(Box)(({ theme, index }) => ({
  background: `linear-gradient(150deg, ${
    index % 2 === 0 ? '#fc466b, #ffc371' : '#ffc371, #ff5f6d'
  } 50%)`,
  boxShadow: 'rgba(0, 0, 0, 0.24) 0px 3px 8px',
  padding: '20px',
  width: '230px',
  '& .userName': {
    fontWeight: 700,
    fontSize: '20px',
    height: '40px',
  },
  [theme.breakpoints.up('md')]: {
    width: '250px',
    display: 'flex',
    flexDirection: 'column',
    gap: '10px',
    '& .userName': {
      fontSize: '30px',
      height: '80px',
    },
  },
}));

const Row = styled(Box)({
  display: 'flex',
  '& .label': {
    width: '50px',
  },
});
