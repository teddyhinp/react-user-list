import { orderOptions, pageSizeOptions, sortOptions } from '@/constants';
import styled from '@emotion/styled';
import { Box, MenuItem, Stack, TextField } from '@mui/material';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Pagination from '@mui/material/Pagination';
export default function Controller() {
  const router = useRouter();
  const [sortBy, setSortBy] = useState('name');
  const [order, setOrder] = useState('asc');
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);

  const pushQs = () => {
    router.push({
      pathname: '/',
      query: { ...router.query, page, size, sortBy, order },
    });
  };

  const handleSortByChange = (e) => {
    setSortBy(e.target.value);
  };
  const handleOrderChange = (e) => {
    setOrder(e.target.value);
  };
  const handlePageChange = (e, page) => {
    setPage(page);
  };
  const handleSizeChange = (e) => {
    setSize(e.target.value);
  };

  useEffect(() => {
    pushQs();
  }, [sortBy, order, page, size, router.query.totoalPage]);

  return (
    <ControllerContainer flex={2}>
      <Stack alignItems="center" gap="20px">
        <FirstRow>
          <Control
            value={sortBy}
            label="Sort By"
            onChange={handleSortByChange}
            options={sortOptions}
          />
          <Control
            value={order}
            label="Order"
            onChange={handleOrderChange}
            options={orderOptions}
          />
          <Control
            value={size}
            label="Page size"
            onChange={handleSizeChange}
            options={pageSizeOptions}
          />
        </FirstRow>
      </Stack>
      <Pagination
        count={+router.query.totalPage || 1}
        page={page}
        onChange={handlePageChange}
      />
    </ControllerContainer>
  );
}

function Control({ value, label, onChange, options }) {
  return (
    <LabelAndDropdown>
      <Box flex={1}>{label}</Box>
      <TextField select value={value} onChange={onChange}>
        {options.map((option) => (
          <MenuItem key={option.value} value={option.value}>
            {option.label}
          </MenuItem>
        ))}
      </TextField>
    </LabelAndDropdown>
  );
}

const LabelAndDropdown = styled(Box)({
  display: 'flex',
  gap: '10px',
  alignItems: 'center',
});

const ControllerContainer = styled(Box)(({ theme }) => ({
  display: 'flex',
  flexDirection: 'column',
  marginTop: '20px',
  flex: 2,
  justifyContent: 'center',
  [theme.breakpoints.up('md')]: {
    alignItems: 'center',
    flexDirection: 'row',
  },
}));

const FirstRow = styled(Box)(({ theme }) => ({
  display: 'flex',
  gap: '5px',
  flexDirection: 'column',
  marginBottom: '10px',
  [theme.breakpoints.up('md')]: {
    flexDirection: 'row',
    gap: '20px',
    marginBottom: '0px',
  },
}));
