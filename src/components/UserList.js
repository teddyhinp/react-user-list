import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Box } from '@mui/material';
import styled from '@emotion/styled';
import UserCard from './UserCard';
import { useRouter } from 'next/router';

export default function UserDatagrid() {
  const router = useRouter();
  const [users, setUsers] = useState([]);
  const fetchUsers = async () => {
    try {
      const { data } = await axios('/api/users', {
        params: router.query,
      });
      setUsers(data.content);
      router.push({
        pathname: '/',
        query: { ...router.query, totalPage: data.totalPage },
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (
      router.query.sortBy &&
      router.query.order &&
      router.query.page &&
      router.query.size
    ) {
      fetchUsers();
    }
  }, [
    router.query.sortBy,
    router.query.order,
    router.query.page,
    router.query.size,
  ]);

  return (
    <Box flex={13} paddingTop="20px">
      <ListContainer>
        {users.map((el, i) => (
          <UserCard detail={el} key={i} index={i} />
        ))}
      </ListContainer>
    </Box>
  );
}

const ListContainer = styled(Box)({
  display: 'flex',
  gap: '20px',
  maxWidth: '1200px',
  flexWrap: 'wrap',
  justifyContent: 'center',
});
