import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  breakpoints: {
    values: { xs: 500, md: 905, lg: 1280 },
  },
  components: {
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          fontSize: '14px',
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          '& .MuiInputBase-root': {
            height: '40px',
          },
        },
      },
    },
  },
});
