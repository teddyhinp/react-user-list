

## Getting Started

First, install the required package by

```bash
npm install
# or
yarn install
```

Then, you can run it in dev mode, and open <http://localhost:3000>
```bash
npm run dev
# or
yarn dev
```
